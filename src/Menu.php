<?php

namespace Multoo\PureCss;

class Menu
{
    public static $hasPermissionCallback = '\Multoo\HqApi\Auth::hasPermission';

    protected static function levelToHtml($level, $selectedName)
    {
        if (isset($level['permission']) && !empty($level['permission'])) {
            if (!is_callable(self::$hasPermissionCallback)) {
                throw new \Exception(self::$hasPermissionCallback . ' is needed tu use \Multoo\PureCss\Menu::levelToHtml with permissions');
            }


            if (call_user_func(self::$hasPermissionCallback, $level['permission'], false) !== true) {
                return null;
            }
        }

        if (isset($level['output']) && $level['output'] === false) {
            return null;
        }

        $tmpOutput = '<li class="pure-menu-item ' . (isset($level['children']) && !empty($level['children']) ? 'pure-menu-has-children pure-menu-allow-hover' : null) . ' ' . (isset($level['name']) && $selectedName !== null && $selectedName === $level['name'] ? 'pure-menu-selected' : null) . ' ' . (isset($level['class']) ? $level['class'] : null) . '"';
        if (isset($level['id'])) {
            $tmpOutput .= ' id="' . $level['id'] . '"';
        }
        if (isset($level['name'])) {
            $tmpOutput .= ' data-pcm-name="' . $level['name'] . '"';
        }
        if (isset($level['v-show'])) {
            $tmpOutput .= ' v-show="' . $level['v-show'] . '"';
        }
        if (isset($level['v-hide'])) {
            $tmpOutput .= ' v-hide="' . $level['v-hide'] . '"';
        }
        $tmpOutput .= '>';

        $tmpOutput .= '<a class="pure-menu-link ' . (isset($level['extraClasses']) && !empty($level['extraClasses']) ? implode(' ', $level['extraClasses']) : null) . '"';
        $tmpOutput .= 'href="' . ($level['url'] ?? '#') . '"';
        if (isset($level['onClick'])) {
            $tmpOutput .= ' onClick="' . $level['onClick'] . '"';
        }
        if (isset($level['v-on:click'])) {
            $tmpOutput .= ' v-on:click="' . $level['v-on:click'] . '"';
        }
        $tmpOutput .= '>' . $level['title'] . '</a>';

        $hasChildrenWithContent = false;

        if (isset($level['children']) && !empty($level['children'])) {
            $tmpChildrenOutput = '<ul class="pure-menu-children">';
            foreach ($level['children'] as $childLevel) {
                if ($tmpChildOutput = self::levelToHtml($childLevel, $selectedName)) {
                    $hasChildrenWithContent = true;
                }
                $tmpChildrenOutput .= $tmpChildOutput;
            }
            $tmpChildrenOutput .= '</ul>';

            if ($hasChildrenWithContent) {
                $tmpOutput .= $tmpChildrenOutput;
            }
        }

        $tmpOutput .= '</li>';

        return isset($level['url']) || isset($level['onClick']) || isset($level['v-on:click']) || $hasChildrenWithContent ? $tmpOutput : null;
    }

    /**
     * Static method to make the HTML structure for a Pure.CSS menu from an array
     *
     * Example:
     * $menuStructure = [
     *  [
     *      'name' => 'organization',
     *      'title' => 'Organisatie',
     *      'children' => [
     *          ['title' => 'Projecten', 'url' => '/organization/project/overview', 'permission' => ['project_admin', 'project_read']],
     *          ['title' => 'Groepen', 'url' => '/organization/employeeGroup/permission', 'permission' => 'group_admin'],
     *          ['title' => 'Rechten', 'url' => '/organization/project/permission', 'permission' => 'permission_admin'],
     *          ['title' => 'Medewerkers', 'url' => '/organization/employee/overview', 'permission' => ['employee_admin', 'employee_read']],
     *       ]
     *  ],
     *  [
     *      'name' => 'itAndDev',
     *      'title' => 'IT / Development',
     *      'children' => [
     *          ['title' => 'API tokens', 'url' => '/itAndDev/apiToken/overview', 'permission' => 'api_token_admin'],
     *          ['title' => 'IP Whitelist', 'url' => '/itAndDev/ipWhitelist/permission', 'permission' => 'ip_whitelist_admin'],
     *          ['title' => 'IP Blacklist', 'url' => '/itAndDev/ipBlacklist/overview', 'permission' => 'ip_blacklist_admin'],
     *          ['title' => 'IDS log', 'url' => '/itAndDev/idsLog/overview', 'permission' => ['ids_log_admin', 'ids_log_read']],
     *          ['title' => 'Server logs', 'url' => '/itAndDev/serverLog/overview', 'permission' => ['server_log_admin', 'server_log_read']],
     *       ]
     *  ]
     * ];
     *
     * echo \Multoo\PureCss\Menu::build($menuStructure);
     *
     * @param array $menuStructure
     * @param array|null $extraClasses
     * @param null $selectedName
     * @return string
     * @throws \Exception
     */
    public static function build(array $menuStructure, array $extraClasses = null, $selectedName = null)
    {
        $output = '<ul class="pure-menu-list ' . (isset($extraClasses) && !empty($extraClasses) ? implode(' ', $extraClasses) : null) . '">';
        foreach ($menuStructure as $level) {
            $output .= self::levelToHtml($level, $selectedName);
        }
        $output .= '</ul>';
        return $output;
    }
}
